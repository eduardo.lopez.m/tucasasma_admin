<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>{{$nombrePropiedad}}</title>
	</head>
	<body>
		<div align="center">
			<img align="center" src="{{$rutaImg}}/img/logo_tucasasma1_300.png" width="180px" height="80px" />
		</div>
		@foreach($propiedades as $propiedad)
			<h1 align="center">{{$propiedad->ln_nombre}}</h1>
			<h3 align="center">{{$propiedad->ln_numero_casa}} {{$propiedad->ln_direccion}}</h3>
			<h3 align="center">$ {{number_format($propiedad->nu_precio,2,'.',',')}} {{$propiedad->ln_moneda}} </h3>
			<h5 align="center">{{$propiedad->nu_dormitorio}} Habitaciones, {{$propiedad->nu_banio}} Baños, {{$propiedad->nu_metros_terreno}} M2</h5>
			<h5 align="center"> Tipo de Inmueble: {{$propiedad->ln_tipo_inmueble}},  Tipo Operación: {{$propiedad->ln_tipo_operacion}}, M2 de Vivienda: {{$propiedad->nu_metros_vivienda}}</h5>
			<img src="{{$rutaImg}}/{{$propiedad->ln_url_imagen}}" width="100%" height="350px" />
			<br>
			<h4>Mas Información</h4>
			<p>{{$propiedad->ln_detalles}}</p>
			<h4>Imágenes y Características Ilustrativas</h4>
			@foreach($imagenes as $imagen)
              <img style="margin-bottom: 5px;" src="{{$rutaImg}}/{{$imagen->ln_url_imagen}}" width="100%" height="315px" />
            @endforeach
		@endforeach
	</body>
</html>