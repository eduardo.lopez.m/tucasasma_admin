    <!-- Footer -->
    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="mb-5">
              <h3 class="footer-heading mb-4" id="idTituloFooter"></h3>
              <p id="idDescripcionFooter"></p>
            </div>
          </div>
          <div class="col-lg-4 mb-5 mb-lg-0">
            <div class="row mb-5">
              <div class="col-md-12">
                <h3 class="footer-heading mb-4">Enlaces Rapidos</h3>
              </div>
              <div class="col-md-6 col-lg-6">
                <ul class="list-unstyled">
                  <li><a href="{{ asset('Inicio')}}">Inicio</a></li>
                  <li><a href="{{ asset('Propiedades')}}">Propiedades</a></li>
                  <li><a href="{{ asset('Asesores')}}">Asesores</a></li>
                </ul>
              </div>
              <div class="col-md-6 col-lg-6">
                <ul class="list-unstyled">
                  <li><a href="{{ asset('Nosotros')}}">Nosotros</a></li>
                  <li><a href="{{ asset('Contacto')}}">Contacto</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-lg-4 mb-5 mb-lg-0">
            <h3 class="footer-heading mb-4">Contactanos</h3>
            <p class="mb-2"><strong>Dirección: </strong> San Miguel de Allende, Guanajuato, México</p>
            <p class="mb-2" id="idTelefonoFooter"></p>
            <p class="mb-2" id="idEmailFooter"></p>
              <div>
                <a href="#" class="pl-0 pr-3" target="_blank" id="idFacebookFooter"><i class="iconfooter icon-facebook-square"></i></a>
                <a href="#" class="pl-3 pr-3" target="_blank" id="idTwitterFooter"><i class="iconfooter icon-youtube-square"></i></a>
                <a href="#" class="pl-3 pr-3" target="_blank" id="idLinkedinFooter"><i class="iconfooter icon-instagram"></i></span></a>
                <a href="#" class="pl-3 pr-3" target="_blank" id="idWhatsappFooter"><i class="iconfooter icon-whatsapp"></i></a>
              </div>
          </div>
        </div>
        <div class="row pt-3 mt-3 text-center">
          <div class="col-md-12">
            <hr>
            <p id="idDerechosFooter"></a>
            </p>
          </div>
        </div>
      </div>
    </footer>