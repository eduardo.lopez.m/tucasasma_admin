<!-- Mainly scripts -->
<!-- <script src="{{ asset('js/jquery-2.1.1.js') }}"></script> -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- Flot -->
<script src="{{ asset('js/plugins/flot/jquery.flot.js') }}"></script>
<script src="{{ asset('js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ asset('js/plugins/flot/jquery.flot.spline.js') }}"></script>
<script src="{{ asset('js/plugins/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('js/plugins/flot/jquery.flot.pie.js') }}"></script>

<!-- Peity -->
<script src="{{ asset('js/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('js/demo/peity-demo.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ asset('js/inspinia.js') }}"></script>
<script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>

<!-- jQuery UI -->
<script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

<!-- GITTER -->
<script src="{{ asset('js/plugins/gritter/jquery.gritter.min.js') }}"></script>

<!-- Sparkline -->
<script src="{{ asset('js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

<!-- Sparkline demo data  -->
<script src="{{ asset('js/demo/sparkline-demo.js') }}"></script>

<!-- ChartJS-->
<script src="{{ asset('js/plugins/chartJs/Chart.min.js') }}"></script>

<!-- Toastr -->
<script src="{{ asset('js/plugins/toastr/toastr.min.js') }}"></script>

<!-- Table -->
<script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>

<!-- Multiselect -->
<script src="{{ asset('js/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>

<!-- Sweet alert -->
<script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<!-- iCheck -->
<script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>

<script src="{{ asset('js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<!-- Input Mask-->
<script src="{{ asset('js/plugins/jasny/jasny-bootstrap.min.js') }}"></script>

<!-- Mainly scripts -->

<script type="text/javascript">
    $(document).ready(function() {
        if (window.location.pathname == "/home") {
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('Bienvenido!', '');
            }, 1300);
        }

        // Formato checkbox general
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        /* FORMATOS PARA INPUT FECHA */
        $('.input-group.date, .input-daterange').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: 'yyyy-mm-dd',
        });

        window.hostGeneral = ""+window.location;
        hostGeneral = hostGeneral.replace(window.location.pathname, "") + "/";

        // if (document.querySelector(".selectGenPais")) {
        //     var data = fnPeticionAjaxGeneral ("GET", "json", hostGeneral+"ws/abcPais", {});
        //     var resp = fnListadoSelectGeneral(".selectGenPais", data);
        // }

        // Formato select general
        fnFormatoSelectGeneral(".selectFormatoGeneral");

        fnObtenerTotalVisitas();
    });

    /**
     * Función para obtener el número de visitas
     * @return {[type]} [description]
     */
    function fnObtenerTotalVisitas() {
        $.get(window.location.origin+"/totalVisitas", function(infoData){
            if(infoData.intState){
                $("#idGenVisitasDia").empty();
                $("#idGenVisitasTotal").empty();

                $("#idGenVisitasDia").append(''+infoData.contenido.hoy);
                $("#idGenVisitasTotal").append(''+infoData.contenido.total);
            } else {
                swal({title: "Información!", text: 'Ocurrio un problema, al obtener el número de visitas.', type: "warning"});
            }
        });
    }

    $(document).on("click",".paginate_button",function() {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });

    function fnAjaxSelect(tipo, ruta, params){
        var url = window.location.origin+"/"+ruta;
        var infoData = [];

        $.ajax({
            async:false,
            url:  url,
            type: tipo,
            dataType: "json",
            data: params,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function(data){
            if(data.intState){
                infoData =  data;
            }else{
                swal({
                    title: "Conexión",
                    text: data.strMensaje,
                    type: "error"
                });
                infoData =  data;
            }
        });

        return infoData;
    }
    
    function number_format (number, decimals, dec_point, thousands_sep) {
        // Strip all characters but numerical ones.
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }

    /* MENSAJE SUCCESS INFORMATIVO LATERAL TIPO TOASTR */
    function fnMensajeInformativoSuccess(strMensaje, timeOut=4000, timeIn=10){
        setTimeout(function() {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: timeOut
            };
            toastr.success(strMensaje, '');
        }, timeIn);
    }

    function fnFormatoSelectGeneral(idClase) {
        $(''+idClase).multiselect({
            enableFiltering: true,
            filterBehavior: 'text',
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '100%',
            numberDisplayed: 1,
            includeSelectAllOption: true
        });

        // Estilos
        $('.multiselect-container').css({
            'max-height': "200px"
        });
        $('.multiselect-container').css({
            'overflow-y': "scroll"
        });
    }

    function fnListadoSelectGeneral(elemento, dataJson, limpiar = 0) {
        var listado = "";

        if ($(""+elemento).prop("multiple")) {
            $(""+elemento).empty();
        }

        if (limpiar == 1) {
            $(""+elemento).empty();
            if (!$(""+elemento).prop("multiple")) {
                listado += "<option value=''>Seleccionar</option>";
            }
        }

        var valorSelect = "";
        if ($(""+elemento).prop("value")) {
            valorSelect = $(""+elemento).prop("value");
        }

        var valor = "";
        var texto = "";
        var contador = 1;
        var selected = '';

        for (var info in dataJson) {
            contador = 1;

            for (var info2 in dataJson[info]) {
                if (contador == 1) {
                    valor = dataJson[info][info2];
                } else {
                    texto = dataJson[info][info2];
                }
                contador ++;
            }

            selected = '';
            if (valorSelect == valor) {
                selected = 'selected="true"';
            }

            listado += "<option value='"+valor+"' "+selected+">"+texto+"</option>";
        }

        $(""+elemento).append(listado);

        fnFormatoSelectGeneral(""+elemento);

        if (limpiar == 1) {
            $(''+elemento).multiselect('rebuild');
        }
    }

    /**
     * Función para validar campos obligatorios del formulario
     * @param  {[type]} idFormulario Id del formulario
     * @param  {[type]} nombreCampo  Array con los datos a validar
     * @return {[type]}              [description]
     */
    function fnValidarFormulario(idFormulario, nombreCampo) {
        var msg = false;
        var params = getParams(idFormulario), campos = getMatchets(idFormulario);

        $.each(nombreCampo,function(index, el) {
            // Eliminar clase
            $("#"+index+"Error").removeClass("has-error");
        });
        
        $.each(campos,function(index, el) {
            if($("#"+el).is(":disabled")){ return; }
            if(nombreCampo[el]===undefined){ return; }
            if(!params.hasOwnProperty(el)){ return; }
            if(params[el]!=0&&params[el]!=-1){ return; }
            msg = true;
            $("#"+el+"Error").addClass("has-error");
        });
        
        if(msg){ swal({title: "Información!", text: 'Campos vacíos', type: "warning"}); }

        return msg;
    }

    /**
     * Función para agregar los nombres
     * @param  {[type]} id [description]
     * @return {[type]}    [description]
     */
    function getMatchets(id) {
        var params = [];
        $('#'+id).find('input[name], select[name], textarea[name]').each(function(index, el) {
            var $self = $(this);
            params.push($self.attr('name'));
        });
        return params;
    }

    /**
     * Función para agregar los nombres
     * @param  {[type]} id [description]
     * @return {[type]}    [description]
     */
    function getParams(id) {
        var params = {};
        $('#'+id).find('input[name], select[name], textarea[name]').each(function(index, el) {
            var $self = $(this);
            //if($self.val() !== undefined) {
                params[$self.attr('name')] = $self.val();
            //}    
        });
        return params;
    }

    /**
     * Clase para solo número en la caja de texto
     * @param  {[type]} event) {        if(event.shiftKey)  event.preventDefault();     if ( $( this ).hasClass( "porcentaje" ) ) {             var chark [description]
     * @return {[type]}        [description]
     */
    $(".soloNumeros").keydown(function(event) {
        // Desactivamos cualquier combinación con shift
        if(event.shiftKey)
        event.preventDefault();
        
        if ( $( this ).hasClass( "porcentaje" ) ) {
            // Si tiene porcentaje, validar
            var chark = String.fromCharCode(event.keyCode);
            var tempValue = $(this).val()+chark;
            if (Number(tempValue) > Number(100)) {
                // Es mayor a 100
                return false;
            }
        }

        /*  
        No permite ingresar pulsaciones a menos que sean los siguientes
        KeyCode Permitidos
        keycode 8 Retroceso
        keycode 37 Flecha Derecha
        keycode 39  Flecha Izquierda
        keycode 46 Suprimir
        */
        //No permite mas de 11 caracteres Numéricos
        if (event.keyCode != 46 && event.keyCode != 8 && event.keyCode != 37 && event.keyCode != 39) 
        if($(this).val().length >= 11)
        event.preventDefault();

        // Solo Numeros del 0 a 9 
        if (event.keyCode < 48 || event.keyCode > 57)
        //Solo Teclado Numerico 0 a 9
        if (event.keyCode < 96 || event.keyCode > 105)
        /*  
        No permite ingresar pulsaciones a menos que sean los siguietes
        KeyCode Permitidos
        keycode 8 Retroceso
        keycode 37 Flecha Derecha
        keycode 39  Flecha Izquierda
        keycode 46 Suprimir
        */
        if(event.keyCode != 46 && event.keyCode != 8 && event.keyCode != 37 && event.keyCode != 39)
        event.preventDefault();

        
    });

    // if (document.querySelector(".selectMoneda")) {
    //     var option = "";

    //     option += "<option value=''>Seleccionar</option>";
    //     option += "<option value='MXN'>MXN</option>";
    //     option += "<option value='USD'>USD</option>";
    //     $(".selectMoneda").append(option);
    //     fnFormatoSelectGeneral(".selectMoneda");
    // }

    if (document.querySelector(".selectMoneda")) {
        var formData = new FormData();
        formData.append("_method", 'GET');
        formData.append("_token", $('input[name="_token"]').attr('value'));
        var infoData = fnAjaxSelect('POST','fnComboMonedas',formData);
        
        if(infoData.intState){
            if(infoData.datos !== null){
                fnListadoSelectGeneral('.selectMoneda', infoData.datos, 1);
                fnListadoSelectGeneral('.selectMoneda', infoData.datos, 1);
            }
        }
    }

    if (document.querySelector(".selectTipoInmueble")) {
        var formData = new FormData();
        formData.append("_method", 'GET');
        formData.append("_token", $('input[name="_token"]').attr('value'));
        var infoData = fnAjaxSelect('POST','fnComboTipoInmuebles',formData);
        
        if(infoData.intState){
            if(infoData.datos !== null){
                fnListadoSelectGeneral('.selectTipoInmueble', infoData.datos, 1);
                fnListadoSelectGeneral('.selectTipoInmueble', infoData.datos, 1);
            }
        }
    }

    if (document.querySelector(".selectTipoOperacion")) {
        var formData = new FormData();
        formData.append("_method", 'GET');
        formData.append("_token", $('input[name="_token"]').attr('value'));
        var infoData = fnAjaxSelect('POST','fnComboTipoOperacion',formData);
        
        if(infoData.intState){
            if(infoData.datos !== null){
                fnListadoSelectGeneral('.selectTipoOperacion', infoData.datos, 1);
                fnListadoSelectGeneral('.selectTipoOperacion', infoData.datos, 1);
            }
        }
    }

    /**
     * Función para obtener los datos configurados de principal
     * @param  {Number} tipo          [description]
     * @param  {[type]} idtel1        [description]
     * @param  {[type]} idtel2        [description]
     * @param  {[type]} idcor1        [description]
     * @param  {[type]} idcor2        [description]
     * @param  {[type]} lnfacebook    [description]
     * @param  {[type]} lntwitter     [description]
     * @param  {[type]} lnlinkedin    [description]
     * @param  {[type]} lnwhatsapp    [description]
     * @param  {[type]} lnwhatsappcod [description]
     * @return {[type]}               [description]
     */
    function fnObtenerInfoPrincipal(tipo = 1, idtel1, idtel2, idcor1, idcor2, lnfacebook, lntwitter, lnlinkedin, lnwhatsapp, lnwhatsappcod) {
        $.get(window.location.origin+"/PrincipalXml", function(xml){
            xml = $( xml );

            var tel1 = xml.find("Telefono").find("Principal").text();
            var tel3 = xml.find("Telefono").find("Secundario").text();
            var cor1 = xml.find("Email").find("Principal").text();
            var cor2 = xml.find("Email").find("Secundario").text();
            var facebook = xml.find("RedesSociales").find("Facebook").text();
            var twitter = xml.find("RedesSociales").find("Twitter").text();
            var linkedin = xml.find("RedesSociales").find("Linkedin").text();
            var whatsApp = xml.find("RedesSociales").find("WhatsApp").text();
            var whatsAppCode = xml.find("RedesSociales").find("WhatsAppCodigo").text();

            $("#"+idtel1).val(""+tel1);
            $("#"+idtel2).val(""+tel3);
            $("#"+idcor1).val(""+cor1);
            $("#"+idcor2).val(""+cor2);
            $("#"+lnfacebook).val(""+facebook);
            $("#"+lntwitter).val(""+twitter);
            $("#"+lnlinkedin).val(""+linkedin);
            $("#"+lnwhatsapp).val(""+whatsApp);
            $("#"+lnwhatsappcod).val(""+whatsAppCode);
        });
    }

    /**
     * Función para obtener los datos configurados de nosotros
     * @param  {Number} tipo     [description]
     * @param  {[type]} idprin   [description]
     * @param  {[type]} idsec    [description]
     * @param  {[type]} iddesc   [description]
     * @param  {[type]} idmision [description]
     * @param  {[type]} idvision [description]
     * @return {[type]}          [description]
     */
    function fnObtenerInfoNosotros(tipo = 1, idprin, idsec, iddesc, idmision, idvision, idImagen) {
        $.get(window.location.origin+"/NosotrosXml", function(xml){
            xml = $( xml );

            var principal = xml.find("Titulos").find("Principal").text();
            var secundario = xml.find("Titulos").find("Secundario").text();
            var descripcion = xml.find("Titulos").find("Descripcion").text();
            var imagen = xml.find("Titulos").find("Imagen").text();
            var mision = xml.find("Empresa").find("Vision").text();
            var vision = xml.find("Empresa").find("Mision").text();

            $("#"+idprin).val(""+principal);
            $("#"+idsec).val(""+secundario);
            $("#"+iddesc).val(""+descripcion);
            $("#"+idImagen).val(""+imagen);
            $("#"+idmision).val(""+mision);
            $("#"+idvision).val(""+vision);
        });
    }

    /**
     * Función para obtener los datos configurados de contacto
     * @param  {Number} tipo   [description]
     * @param  {[type]} idprin [description]
     * @param  {[type]} iddesc [description]
     * @param  {[type]} idmaps [description]
     * @return {[type]}        [description]
     */
    function fnObtenerInfoContacto(tipo = 1, idprin, iddesc, idmaps, idemails) {
        $.get(window.location.origin+"/ContactoXml", function(xml){
            xml = $( xml );

            var principal = xml.find("Titulos").find("Principal").text();
            var descripcion = xml.find("Titulos").find("Descripcion").text();
            var maps = xml.find("Links").find("Maps").text();
            var emails = xml.find("Email").find("Llegada").text();

            $("#"+idprin).val(""+principal);
            $("#"+iddesc).val(""+descripcion);
            $("#"+idmaps).val(""+maps);
            $("#"+idemails).val(""+emails);
        });
    }

    /**
     * Función para obtener los datos configurados del pie de página
     * @param  {Number} tipo   [description]
     * @param  {[type]} idprin [description]
     * @param  {[type]} iddesc [description]
     * @param  {[type]} idmaps [description]
     * @return {[type]}        [description]
     */
    function fnObtenerInfoPiePagina(tipo = 1, idprin, iddesc, idder) {
        $.get(window.location.origin+"/PiePaginaXml", function(xml){
            xml = $( xml );

            var principal = xml.find("Titulos").find("Principal").text();
            var descripcion = xml.find("Titulos").find("Descripcion").text();
            var derechos = xml.find("Derechos").find("Texto").text();

            $("#"+idprin).val(""+principal);
            $("#"+iddesc).val(""+descripcion);
            $("#"+idder).val(""+derechos);
        });
    }

</script>