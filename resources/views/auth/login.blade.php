@extends('layouts.auth')

@section('content')
<style type="text/css">
    body {
        background-image: url("img/login_fondo.jpg");
        background-repeat: no-repeat;
        background-size: 100% 100%;
    }
    p {
        font-weight: bold;
        color: #283B62;
        margin-top: 20px;
        margin-bottom: 20px;
    }
</style>
<form method="POST" action="{{ route('login') }}">
    @csrf
    <!--  -->
    <div class="middle-box text-center loginscreen animated fadeInDown pull-right" style="background: white; margin-right: 50px;">
        <div>
            <div>
                <h1 class="logo-name">
                    <img alt="image" src="{{ asset('img/logo_tucasasma1_230.png') }}" />
                </h1>
            </div>
            <p>INICIO DE SESIÓN</p>
            <form class="m-t" role="form" action="index.html">
                <div class="col-lg-12">
                    <div class="form-group">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="E-mail" style="border-color: #283B62;">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Contraseña" style="border-color: #283B62;">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-success btn-rounded" style="background: #283B62; margin-bottom: 20px;">INGRESAR</button>
                </div>
            </form>
        </div>
    </div>
</form>
@endsection
