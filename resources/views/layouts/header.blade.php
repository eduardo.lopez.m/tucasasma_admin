<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <!-- <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form> -->
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <!-- <i class="fa fa-info-circle"></i> --> Vistas del Día  <span class="label label-primary" id="idGenVisitasDia">0</span>
                </a>
                <!-- <ul class="dropdown-menu dropdown-messages">
                    <li>
                        <div class="text-center link-block">
                            <a href="mailbox.html">
                                <i class="fa fa-envelope"></i> <strong>3 Visitas del Día</strong>
                            </a>
                        </div>
                    </li>
                </ul> -->
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <!-- <i class="fa fa-info-circle"></i> --> Total de Visitas  <span class="label label-primary" id="idGenVisitasTotal">0</span>
                </a>
                <!-- <ul class="dropdown-menu dropdown-messages">
                    <li>
                        <div class="text-center link-block">
                            <a href="mailbox.html">
                                <i class="fa fa-envelope"></i> <strong>Total de Visitas 240</strong>
                            </a>
                        </div>
                    </li>
                </ul> -->
            </li>
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out"></i> Cerrar Sesión
                </a>
            </li>
        </ul>
    </nav>
</div>

    