@extends('layouts.app')
@section('content')

    <!-- Apartado del Breadcrumb -->
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <br>
            <ol class="breadcrumb">
                <li>
                    <a href="">Configuración Principal</a>
                </li>
                <!-- <li>
                    <a href="">Carrusel Conoce SMA</a>
                </li> -->
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <!-- campos de criterios de busqueda -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <!-- FILTROS -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Información</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <!-- <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a> -->
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form id="frmFormulario" action="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Teléfono</h2>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" id="txtTelPrincipalError">
                                        <label>Principal</label> 
                                        <input name="txtTelPrincipal"  type="text" id="txtTelPrincipal" placeholder="Ejemplo: +52 (415) 153 32 08" class="form-control" title="Teléfono Principal">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" id="txtTelSecundarioError">
                                        <label>Secundario</label> 
                                        <input name="txtTelSecundario"  type="text" id="txtTelSecundario" placeholder="Ejemplo: +52 (415) 153 32 08" class="form-control" title="Teléfono Secundario">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Email</h2>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" id="txtEmailPrincipalError">
                                        <label>Principal</label> 
                                        <input name="txtEmailPrincipal"  type="text" id="txtEmailPrincipal" placeholder="Ejemplo: tucasasma@gmail.com" class="form-control" title="Email Principal">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" id="txtEmailSecundarioError">
                                        <label>Secundario</label> 
                                        <input name="txtEmailSecundario"  type="text" id="txtEmailSecundario" placeholder="Ejemplo: tucasasma@gmail.com" class="form-control" title="Email Secundario">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Redes Sociales</h2>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group m-b">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary"><span class="fa fa-facebook-square"></span></button> 
                                        </span> 
                                        <input name="txtLinkFacebook"  type="text" id="txtLinkFacebook" placeholder="Ejemplo: https://www.facebook.com/" class="form-control" title="Link de Facebook">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group m-b">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary"><span class="fa fa-youtube"></span></button> 
                                        </span> 
                                        <input name="txtLinkTwitter"  type="text" id="txtLinkTwitter" placeholder="Ejemplo: https://www.youtube.com/" class="form-control" title="Link Twitter">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group m-b">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary"><span class="fa  fa-instagram"></span></button> 
                                        </span> 
                                        <input name="txtLinkLinkedin"  type="text" id="txtLinkLinkedin" placeholder="Ejemplo: https://www.instagram.com/" class="form-control" title="Link Instagram">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group m-b" id="txtLinkWhatsCodError">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary">WhatsApp Código +</button> 
                                        </span> 
                                        <input name="txtLinkWhatsCod" type="text" id="txtLinkWhatsCod" placeholder="Ejemplo: 52" class="form-control soloNumeros" value="52" maxlength="2" title="WhatsApp Código">
                                    </div>
                                    <div class="input-group m-b" id="txtLinkWhatsError">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary"><span class="fa fa fa-whatsapp"></span></button> 
                                        </span> 
                                        <input name="txtLinkWhats"  type="text" id="txtLinkWhats" placeholder="Ejemplo: 1234567890" class="form-control soloNumeros" maxlength="10" title="WhatsApp Número">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="ibox-footer" style="text-align: center;">
                        <button id="btnGuardar" class="btn btn-success btn-sm" type="button"><i class="fa fa-save"></i>&nbsp;&nbsp;<span class="bold">Guardar</span></button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src="{{ asset('ajax/principal.js') }}"></script>

@endsection