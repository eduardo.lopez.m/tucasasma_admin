$(document).ready(function () {

    $('#mdlAdminPrincipal').addClass('active');

    $("#btnGuardar").click(function(){
        fnGuardarPrincipal();
    });

    fnObtenerInfoPrincipal(1, 'txtTelPrincipal', 'txtTelSecundario', 'txtEmailPrincipal', 'txtEmailSecundario', 'txtLinkFacebook', 'txtLinkTwitter', 'txtLinkLinkedin', 'txtLinkWhats', 'txtLinkWhatsCod');

});

/**
 * Función para guardar la información de la configuración
 * @return {[type]} [description]
 */
function fnGuardarPrincipal() {
    var formData = new FormData(document.getElementById("frmFormulario"));
    formData.append("_method", 'POST');

    $("#txtLinkWhatsCodError").removeClass("has-error");
    $("#txtLinkWhatsError").removeClass("has-error");
    

    var nombreCampo = {
        'txtTelPrincipal':'teléfono del encabezado',
        'txtTelSecundario':'teléfono del pie de página',
        'txtEmailPrincipal':'email del encabezado',
        'txtEmailSecundario':'email del pie de página'
    };

    var msg = fnValidarFormulario('frmFormulario', nombreCampo);
    if(msg){ return; }

    if ($("#txtLinkWhatsCod").val().trim() != '' || $("#txtLinkWhats").val().trim() != '') {
        var whatscode = $("#txtLinkWhatsCod").val().trim();
        var whatsnum = $("#txtLinkWhats").val().trim();
        
        if (whatscode.length != 2 && whatscode.length != 3) {
            $("#txtLinkWhatsCodError").addClass("has-error");
            swal({title: "Información!", text: 'El código de whatsApp debe ser de 2 o 3 dígitos.', type: "warning"});
            return;
        }

        if (whatsnum.length != 10) {
            $("#txtLinkWhatsError").addClass("has-error");
            swal({title: "Información!", text: 'El número de whatsApp debe ser de 10 dígitos.', type: "warning"});
            return;
        }
    }

    var infoData = fnAjaxSelect('POST','principalAjax',formData);
    if(infoData.intState){
        swal({title: "Información!", text: infoData.strMensaje, type: "success"});
    } else {
        swal({title: "Información!", text: 'Ocurrio un problema, verifica con el administrador.', type: "warning"});
    }
}