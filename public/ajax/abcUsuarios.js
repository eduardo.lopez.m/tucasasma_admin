$(document).ready(function () {

    $('#mdlUsuarios').addClass('active');
    
    /* Configuracion de tabla */
	$('.dataTables-example').DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Usuarios'},
            {extend: 'pdf', title: 'Usuarios'},
            {extend: 'print',
                customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
            }
        ],
        //pageLength : 1000,
        columnDefs: [
            { className: 'text-center', targets: [4, 5] },
        ]
    });

    fnObtenerUsuarios();

    $("#btnAgregarNuevo").click(function(){
        fnAgregarRegistro();
    });

    $("#btnModificarRegistro").click(function(){
        fnModificarRegistro($("#id").val());
    });

    $("#btnBuscar").click(function(){
        fnObtenerUsuarios();
    });

    $('#mdlFormulario').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var title = "";

        $("#btnAgregarNuevo").css("display","none");
        $("#btnModificarRegistro").css("display","none");

        if(button.data('id')>0){
            title="Modificar Usuario";
            $("#btnModificarRegistro").css("display","");
            fnEditarRegistro(button.data('id'));
        }else{
            title="Nuevo Usuario";
            $("#btnAgregarNuevo").css("display","");
            $("#id").val("");
            $("#name").val("");
            $("#ln_direccion").val("");
            $("#ln_telefono").val("");
            $("#email").val("");
            $("#password").val("");
            $("#nu_activo").prop('checked',true).iCheck('update');
        }
        
        modal.find('.modal-title').text('' + title)
        
    });
});

$(document).on("ifChanged",".chkEstatus",function() {
    var nu_activo = 0;
    
    if($(this).prop('checked')){
        nu_activo = 1;
    }

    fnModificarEstatus($(this).data('id'), nu_activo);
});

$(document).on("click",".btnEliminar",function() {
    var id = $(this).data('id');
    swal({
        title: "¿Desea eliminar el usuario?",
        text: "No podrás recuperar la información!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false
    }, function () {
        fnEliminarRegistro(id);
    });
    
});

function fnEditarRegistro(id){
    var url = 'Users/'+id;
    var formData = new FormData();

    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('GET',url,formData);
    
    if(infoData.intState){
        if(infoData.usuario !== null){
            $.each(infoData.usuario,function(index, el) {
                $("#id").val(el.id);
                $("#name").val(el.name);
                $("#ln_direccion").val(el.ln_direccion);
                $("#ln_telefono").val(el.ln_telefono);
                $("#email").val(el.email);
                $("#password").val(el.passwordv);

                $("#nu_activo").prop('checked',false).iCheck('update');
                if(el.nu_activo=="1"){
                    $("#nu_activo").prop('checked',true).iCheck('update');
                }
            });
        }
    }
}

function fnAgregarRegistro(){
    var url = 'Users';
    var formData = new FormData(document.getElementById("frmFormulario"));
    
    formData.append("_method", 'POST');

    var infoData = fnAjaxSelect('POST',url,formData);
    
    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto!", text: infoData.strMensaje, type: "success"});
        fnObtenerUsuarios();
    }
}

function fnEliminarRegistro(id){
    var formData = new FormData();
    var url = 'Users/'+id;

    formData.append("_method", 'DELETE');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        swal("Eliminado!", infoData.strMensaje, "success");
        fnObtenerUsuarios();
    }
}

function fnModificarRegistro(id){
    var url = 'Users/'+id;
    var formData = new FormData(document.getElementById("frmFormulario"));

    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto", text: infoData.strMensaje, type: "success"});
        fnObtenerUsuarios();
    }
}

function fnModificarEstatus(id,nu_activo){
    var url = 'Users/'+id;
    var formData = new FormData();
    
    formData.append("nu_activo", nu_activo);
    formData.append("_token", $('input[name="_token"]').attr('value'));
    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        fnMensajeInformativoSuccess(infoData.strMensaje);
    }
}

function fnObtenerUsuarios(){
    var formData = new FormData();
    formData.append("name", $('#txtNombre').val());
    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));
    var infoData = fnAjaxSelect('POST','Users',formData);

    if(infoData.intState){
        if(infoData.conductores !== null){
            var estatus="";
            var btnEliminar="";
            var btnModificar="";
            var chkEstatus="";

            if($('#tblUsuarios').DataTable().data().length > 0){
                $('#tblUsuarios').DataTable().clear();
                $('#tblUsuarios').DataTable().draw();
            }

            $.each(infoData.usuarios,function(index, el) {
                
                chkEstatus="checked";
                
                if(el.nu_activo == 0){
                    chkEstatus="";
                }

                estatus = '<input type="checkbox" name="nu_activo" class="i-checks chkEstatus" data-id="'+el.id+'" style="position: absolute; opacity: 0;" '+chkEstatus+'>';
                btnModificar='<button class="btn btn-primary btn-xs btnModificar" type="button" data-toggle="modal" data-target="#mdlFormulario" data-id="'+el.id+'"><i class="fa fa-edit"></i></button>';
                btnEliminar='<button class="btn btn-danger btn-xs btnEliminar" type="button" data-id="'+el.id+'"><i class="fa fa-trash"></i></button>';

                $('#tblUsuarios').dataTable().fnAddData( 
                    [
                        el.name, 
                        el.ln_direccion, 
                        el.ln_telefono, 
                        el.email, 
                        estatus,
                        btnModificar +" "+btnEliminar
                    ]
                );
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        }
    }
}