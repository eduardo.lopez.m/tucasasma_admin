<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropiedadesImagenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propiedades_imagenes', function (Blueprint $table) {
            $table->bigIncrements('nu_imagen');
            $table->smallInteger('nu_propiedad');
            $table->string('ln_url_imagen');
            $table->string('ln_nombre_imagen');
            $table->smallInteger('nu_principal');
            $table->smallInteger('nu_activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propiedades_imagenes');
    }
}
