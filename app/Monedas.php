<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monedas extends Model
{
    protected $fillable = ['ln_codigo_moneda','ln_desc_moneda','nu_activo'];
    protected $primaryKey = 'nu_moneda';
    protected $hidden = ['updated_at', 'created_at'];
}
