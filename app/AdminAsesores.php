<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminAsesores extends Model
{
    protected $fillable = ['ln_nombre','ln_puesto','ln_descripcion','ln_link_facebook','ln_link_twitter','ln_link_linked','ln_link_whattsapp','nu_cod_pais','ln_telefono','ln_correo','ln_url_imagen','nu_activo'];
    protected $primaryKey = 'nu_asesor';
    protected $hidden = ['updated_at', 'created_at'];

    public function scopeLnNombre($query, $ln_nombre = '') {
    	return $query->where('ln_nombre', 'like', '%'.$ln_nombre.'%');
    }

    public function scopeNuActivo($query, $nu_activo = '') {
        if (!empty($nu_activo) and !is_null($nu_activo)) {
    	    return $query->where('nu_activo', $nu_activo);
        }
    }
}
