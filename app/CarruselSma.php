<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarruselSma extends Model
{
    protected $fillable = ['ln_titulo', 'ln_descripcion', 'ln_url_imagen', 'nu_activo'];
    protected $primaryKey = 'nu_publicidad';
    protected $hidden = ['updated_at', 'created_at'];


    public function scopeLnTitulo($query, $ln_titulo = '') {
    	return $query->where('ln_titulo', 'like', '%'.$ln_titulo.'%');
    }

    public function scopeNuActivo($query, $nu_activo = '') {
        if (!empty($nu_activo) and !is_null($nu_activo)) {
    	    return $query->where('nu_activo', $nu_activo);
        }
    }
}
