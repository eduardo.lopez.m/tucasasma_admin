<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropiedadesImagenes extends Model
{
    protected $fillable = ['nu_propiedad','ln_url_imagen','ln_nombre_imagen','nu_principal','nu_activo'];
    protected $primaryKey = 'nu_imagen';
    protected $hidden = ['updated_at', 'created_at'];
}
