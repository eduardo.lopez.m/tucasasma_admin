<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitas extends Model
{
    protected $table = 'visitas';

    protected $fillable = ['ip'];
    protected $primaryKey = 'id';
    protected $hidden = ['updated_at', 'created_at'];

    public function scopeIp($query, $ip = '') {
        if (!empty($ip) and !is_null($ip)) {
    	    return $query->where('ip', $ip);
        }
    }

    public function scopeFecha($query, $created_at = '') {
        if (!empty($created_at) and !is_null($created_at)) {
    	    return $query->where('created_at', 'LIKE', $created_at.'%');
        }
    }

}
