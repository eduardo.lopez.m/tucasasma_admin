<?php

namespace App\Http\Controllers;

use App\CarruselSma;
use Illuminate\Http\Request;

class CarruselSmaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataRequest = request()->all();
        $publicidad = CarruselSma::LnTitulo($dataRequest['ln_titulo'])
                                    ->NuActivo($dataRequest['nu_activo'])
                                    ->get();
        return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvieron las publicidad","publicidad"=>$publicidad],200) ;            
    }

    public function fnServicioCarruselSMA()
    {
        $publicidad = CarruselSma::NuActivo(1)
                                    ->orderBy('nu_publicidad', 'asc')
                                    ->get();
        return response()->json(["intState"=>1,"publicidad"=>$publicidad],200) ;            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataRequest = request()->all();

        $directorio = 'img-conoce-sma/';
        $arrExtenciones["image/png"] = ".png";
        $arrExtenciones["image/jpg"] = ".jpg";
        $arrExtenciones["image/jpeg"] = ".jpeg";
        
        $estatus=0;
        if(isset($dataRequest['nu_activo'])){
            $estatus=1;
        }

        $ln_url_imagen = $directorio ."sinImagen.png";
        if(isset($dataRequest['ln_url_imagen'])){
            $ln_url_imagen=$directorio.date('Ymd_His').$arrExtenciones[$_FILES['ln_url_imagen']['type']];
            if(!move_uploaded_file($_FILES['ln_url_imagen']['tmp_name'], $ln_url_imagen)){
                $ln_url_imagen = $directorio ."sinImagen.png";
            }
        }
        
        $publicidad=CarruselSma::create([
            "ln_titulo" => $dataRequest['ln_titulo'], 
            "ln_descripcion" => $dataRequest['ln_descripcion'], 
            "ln_url_imagen" => $ln_url_imagen, 
            "nu_activo" => $estatus
        ]);

        return response()->json(["intState"=>1,"strMensaje"=>"Se creó correctamente: ". $dataRequest['ln_titulo'],"publicidad"=>compact('publicidad')],200) ;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarruselSma  $carruselSma
     * @return \Illuminate\Http\Response
     */
    public function show(CarruselSma $carruselSma, $nu_publicidad)
    {
        $publicidad = CarruselSma::where('nu_publicidad', $nu_publicidad)->first();
        return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvó correctamente","publicidad"=>compact("publicidad")],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarruselSma  $carruselSma
     * @return \Illuminate\Http\Response
     */
    public function edit(CarruselSma $carruselSma)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarruselSma  $carruselSma
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CarruselSma $carruselSma, $nu_publicidad)
    {
        $dataRequest = request()->all();

        if(isset($dataRequest['ln_url_imagen'])){
            $directorio = 'img-conoce-sma/';
            $arrExtenciones["image/png"] = ".png";
            $arrExtenciones["image/jpg"] = ".jpg";
            $arrExtenciones["image/jpeg"] = ".jpeg";
            $ln_url_imagen=$directorio.date('Ymd_His').$arrExtenciones[$_FILES['ln_url_imagen']['type']];
            if(!move_uploaded_file($_FILES['ln_url_imagen']['tmp_name'], $ln_url_imagen)){
                $ln_url_imagen = $directorio ."sinImagen.png";
            }
            $dataRequest['ln_url_imagen']=$ln_url_imagen;
        }
        

        if (isset($dataRequest['nu_activo'])) {
            if(!is_numeric($dataRequest['nu_activo'])){
                $dataRequest['nu_activo'] = '1';
            }
        }

        $publicidad = CarruselSma::where('nu_publicidad', $nu_publicidad)->first();

        $publicidad->update($dataRequest);
        return response()->json(["intState"=>1,"strMensaje"=>"Se modificó correctamente a: ".$publicidad->ln_titulo,"publicidad"=>compact('publicidad')],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarruselSma  $carruselSma
     * @return \Illuminate\Http\Response
     */
    public function destroy(CarruselSma $carruselSma, $nu_publicidad)
    {
        $publicidad = CarruselSma::findOrFail($nu_publicidad);
        $publicidad->delete();
        return response()->json(["intState"=>1,"strMensaje"=>"Se eliminó correctamente: ".$publicidad->ln_titulo,"publicidad"=>$publicidad],200);
    }
}
