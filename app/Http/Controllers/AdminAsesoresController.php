<?php

namespace App\Http\Controllers;

use App\AdminAsesores;
use Illuminate\Http\Request;

class AdminAsesoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataRequest = request()->all();
        $asesores = AdminAsesores::LnNombre($dataRequest['ln_nombre'])
                                    ->NuActivo($dataRequest['nu_activo'])
                                    ->get();
        return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvieron los asesores","asesores"=>$asesores],200) ;            
    }

    public function fnServicioAsesores()
    {
        $asesores = AdminAsesores::NuActivo(1)
                                    ->orderBy('nu_asesor', 'asc')
                                    ->get();
        return response()->json(["intState"=>1,"asesores"=>$asesores],200) ;            
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataRequest = request()->all();

        $directorio = 'img-conoce-sma/';
        $arrExtenciones["image/png"] = ".png";
        $arrExtenciones["image/jpg"] = ".jpg";
        $arrExtenciones["image/jpeg"] = ".jpeg";
        
        $estatus=0;
        if(isset($dataRequest['nu_activo'])){
            $estatus=1;
        }

        $ln_url_imagen = $directorio ."sinImagen.png";
        if(isset($dataRequest['ln_url_imagen'])){
            $ln_url_imagen=$directorio.date('Ymd_His').$arrExtenciones[$_FILES['ln_url_imagen']['type']];
            if(!move_uploaded_file($_FILES['ln_url_imagen']['tmp_name'], $ln_url_imagen)){
                $ln_url_imagen = $directorio ."sinImagen.png";
            }
        }
        
        $asesor=AdminAsesores::create([
            'ln_nombre' => $dataRequest['ln_nombre'],
            'ln_puesto' => $dataRequest['ln_puesto'],
            'ln_descripcion' => $dataRequest['ln_descripcion'],
            'ln_link_facebook' => $dataRequest['ln_link_facebook'],
            'ln_link_twitter' => $dataRequest['ln_link_twitter'],
            'ln_link_linked' => $dataRequest['ln_link_linked'],
            'ln_link_whattsapp' => $dataRequest['ln_link_whattsapp'],
            'ln_telefono' => $dataRequest['ln_telefono'],
            'nu_cod_pais' => $dataRequest['nu_cod_pais'],
            'ln_correo' => $dataRequest['ln_correo'],
            'ln_url_imagen' => $ln_url_imagen,
            'nu_activo' => $estatus
        ]);

        return response()->json(["intState"=>1,"strMensaje"=>"Se creó correctamente: ". $dataRequest['ln_nombre'],"asesor"=>compact('asesor')],200) ;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdminAsesores  $adminAsesores
     * @return \Illuminate\Http\Response
     */
    public function show(AdminAsesores $adminAsesores, $nu_asesor)
    {
        $asesor = AdminAsesores::where('nu_asesor', $nu_asesor)->first();
        return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvó correctamente","asesor"=>compact("asesor")],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminAsesores  $adminAsesores
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminAsesores $adminAsesores, $nu_asesor)
    {
        $dataRequest = request()->all();

        if(isset($dataRequest['ln_url_imagen'])){
            $directorio = 'img-conoce-sma/';
            $arrExtenciones["image/png"] = ".png";
            $arrExtenciones["image/jpg"] = ".jpg";
            $arrExtenciones["image/jpeg"] = ".jpeg";
            $ln_url_imagen=$directorio.date('Ymd_His').$arrExtenciones[$_FILES['ln_url_imagen']['type']];
            if(!move_uploaded_file($_FILES['ln_url_imagen']['tmp_name'], $ln_url_imagen)){
                $ln_url_imagen = $directorio ."sinImagen.png";
            }
            $dataRequest['ln_url_imagen']=$ln_url_imagen;
        }
        
        if (isset($dataRequest['nu_activo'])) {
            if(!is_numeric($dataRequest['nu_activo'])){
                $dataRequest['nu_activo'] = '1';
            }
        }

        $asesor = AdminAsesores::where('nu_asesor', $nu_asesor)->first();

        $asesor->update($dataRequest);

        return response()->json(["intState"=>1,"strMensaje"=>"Se modificó correctamente a: ".$asesor->ln_nombre,"asesor"=>compact('asesor')],200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminAsesores  $adminAsesores
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminAsesores $adminAsesores, $nu_asesor)
    {
        $asesor = AdminAsesores::findOrFail($nu_asesor);
        $asesor->delete();
        return response()->json(["intState"=>1,"strMensaje"=>"Se eliminó correctamente: ".$asesor->ln_titulo,"asesor"=>$asesor],200);
    }
}
