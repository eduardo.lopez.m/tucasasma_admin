<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminPropiedades extends Model
{
    protected $fillable = ['ln_nombre','ln_numero_casa','nu_dormitorio','nu_banio','nu_cochera','nu_metros_terreno','nu_metros_vivienda','nu_tipo_inmueble','nu_tipo_operacion','nu_precio','ln_moneda','ln_correo', 'ln_telefono','ln_detalles','ln_direccion','ln_url_mapa','nu_principal','nu_activo'];
    protected $primaryKey = 'nu_propiedad';
    protected $hidden = ['updated_at', 'created_at'];

    public function scopeLnNombre($query, $ln_nombre = '') {
    	return $query->where('ln_nombre', 'like', '%'.$ln_nombre.'%');
    }

    public function scopeLnMoneda($query, $ln_moneda = '') {
        if($ln_moneda !="" and !is_null($ln_moneda)){
            return $query->where('ln_moneda', 'like', $ln_moneda);
        }
    }

    public function scopeNuTipoInmueble($query, $nu_tipo_inmueble = '') {
        if($nu_tipo_inmueble !="" and $nu_tipo_inmueble !="-1" and !is_null($nu_tipo_inmueble)){
            return $query->where('admin_propiedades.nu_tipo_inmueble', $nu_tipo_inmueble);
        }
    }

    public function scopeNuTipoOperacion($query, $nu_tipo_operacion = '') {
        if($nu_tipo_operacion !="" and $nu_tipo_operacion !="-1" and !is_null($nu_tipo_operacion)){
            return $query->where('admin_propiedades.nu_tipo_operacion', $nu_tipo_operacion);
        }
    }

    public function scopeOrdenarPrecio($query, $ordenar = '') {
        if($ordenar !="" and $ordenar !="-1" and !is_null($ordenar)){
            if($ordenar == '1'){
                return $query->orderBy('admin_propiedades.nu_precio', 'ASC');
            }else{
                return $query->orderBy('admin_propiedades.nu_precio', 'DESC');
            }
        }
    }

    public function scopePropiedadesImagen($query) {
        $subQuerry = PropiedadesImagenes::select('ln_url_imagen')
            ->whereColumn('propiedades_imagenes.nu_propiedad', 'admin_propiedades.nu_propiedad')
            ->where('propiedades_imagenes.nu_principal',1) /* 2 canceladas */
            ->groupBy('propiedades_imagenes.ln_url_imagen');
            
        $query->addSelect([
            "ln_url_imagen"=>$subQuerry
        ]);
    }

}
